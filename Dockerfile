FROM python:3.8.10

RUN python3 -m pip install pylint

RUN pip install nose2 nose2[coverage_plugin]
